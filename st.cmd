require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM13")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-210")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "PBI-BPM13:Ctrl-EVR-201:")
epicsEnvSet("SYSTEM1_PREFIX", "A2T-010LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "A2T 010 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "A2T-020LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "A2T 020 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

